var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001;

var bodyParser = require('body-parser');
app.use(bodyParser.json())
app.use(function(req, res, next) {
 res.header("Access-Control-Allow-Origin", "*");
 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 next();
});
var requestjson = require('request-json');

var path = require('path');
var apiKey = "k_z1o97qvqYdpYkwKd_VwkClOJXr9OH-";
var urlBase = "https://api.mlab.com/api/1/databases/bdbanca2rrh/collections/";
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function(req,res){
  res.sendFile(path.join(__dirname,'index.html'));
})

app.post("/login",function(req,res){
  var id = req.body.user;
  var password = req.body.password;
  var query = 'q={"email":"'+id+'"}'
  var clienteMLab = requestjson.createClient(urlBase+"usuarios?apiKey="+apiKey+"&"+query);

  clienteMLab.get('',function(err, resM, body){
    if(err){
      console.log(body)
    }else{
      if(body.length<1){
        res.status(404).send('El cliente ingresado no esta registrado');
      }else{
        if(id== body[0].email && password==body[0].password){
          res.json({ user:body[0].userId,name: body[0].name,token:body[0].token});
        }else{
            res.status(403).send('Los datos ingresados no son válidos');
        }
      }
    }
  })
})

app.get("/cuentas/:id",function(req,res){
  var id = req.params.id;
  var query = 'q={"userId":"'+id+'"}';
  var clienteMLab = requestjson.createClient(urlBase+"cuentas?apiKey="+apiKey+"&"+query);

  clienteMLab.get('',function(err, resM, body){
    if(err){
      console.log(body)
    }else{
      if(body.length<1){
        res.status(404).send('Usuario no registrado');
      }else {
        res.send(body);
      }
    }
  })
})

app.get("/cuentas/movimientos/:id",function(req,res){
  var id = req.params.id;
  var query = 'q={"account":"'+id+'"}';
  var clienteMLab = requestjson.createClient(urlBase+"movimientos?apiKey="+apiKey+"&"+query);

  clienteMLab.get('',function(err, resM, body){
    if(err){
      console.log(body)
    }else{
      if(body.length<1){
        res.status(404).send('Cuenta no registrada');
      }else{
        res.send(body);
      }
    }
  })
})
